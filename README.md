# Hi there!

This repository is setup for the Jupiter Broadcasting Community, by a community member. These challenges are for the [Jupiter Broadcasting](https://www.jupiterbroadcasting.com/) community and entirely community driven. It's easy make a challenge, and offer a reward in Bitcoin Satoshi's with a match going to Jupiter Broadcasting.

## Applications
[Podcast Apps](https://podcastindex.org/apps)
[BlueWallet](https://bluewallet.io/)

### To make a challenge
Make a pull request with a screenshot for proof of you sending a boost to a JB podcast episode and place it into the new challenges folder of this repository. The name of the file should be formatted as it is in the example provided

> nevoyu-challenge-jim-coder-radio-463.png

### Accepting A Challenge
In order for the challenge to be accepted, you must post a text response in the challenges folder of this repository. Otherwise your challenge will be considered declined after 30 days. You must add the following into the text file with a link to the challenge post in this repository. The name of the file should just be the community memebers name.

> I hereby accept this challenge given to me by $CHALLENGERNAME $LINK

### Completing a Challenge
Simply post a screenshot in the completions folder of this repository with the same name as the file challenge showing proof of sats received from the challenger.

### Declined
You can decline a challenge at any time, if one is declined the JB Hosts and Mumble Room on Linux Unplugged has full rights to reissue the challenge to any community member. Any challenge not accept within 30 days will get automatically declined.
To decline a challenge, simply make a pull request copying the same image file in /challenges to /declined

### Accepting Challenges
Some challenges can take a while, to show your progressing on these challenges simply copy the challenge image into the /accepted folder in this repository and make a pull request.

